+++
title = "Skipping Broken Kernel in Fedora"
author = ["Brendan Van Hook"]
date = 2022-10-02T21:00:00-04:00
tags = ["Linux"]
categories = ["Linux"]
draft = false
+++

Sometimes bugs get through. Maybe an update breaks your system and you need to downgrade a package in order to make it useable. Fedora provides a simple command to help with this: `dnf downgrade`. This command will install the latest version of a package that is less than the highest available version. To downgrade the kernel you can issue the following command

```sh
sudo dnf downgrade kernel-core
```

This will downgrade kernel-core and all its direct dependencies. If we run `dnf upgrade`, we will be prompted to update the kernel again since it is now not the latest version. To skip the upgrade for this version we can add it to the exclude list in `/etc/dnf/dnf.conf`.  Note the version and architecture for the package you want to exclude.

```text
[main]
gpgcheck=True
installonly_limit=3
clean_requirements_on_remove=True
best=False
skip_if_unavailable=True
fastestmirror=True
excludepkgs=kernel*-5.19.12-300.fc37.x86_64
```

Note the glob `*` after kernel. This allows us to exclude all of the kernel packages of that version at once. We could just exclude kernel-core, but this would make dnf complain about broken dependencies whenever you tried to upgrade. The upgrade would still work, but excluding all the kernel packages prevents the noise. Once a kernel version is released that is greater than the excluded version, the packages will be upgraded.