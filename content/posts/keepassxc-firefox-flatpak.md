+++
title = "Enabling KeePassXC Add-On for Flatpak Firefox"
author = ["Brendan Van Hook"]
date = 2022-09-06T17:00:00-04:00
tags = ["Linux", "Flatpak"]
categories = ["Linux"]
draft = false
+++

## Abstract {#abstract}

This guide was written for and verified with Fedora 36 Silverblue. It **should** work on other distributions, especially if when using flatpak versions of both Firefox and KeePassXC, but it has only been verified on Fedora 36/37. The only differences would be in the location of the socket (for non-flatpak versions of KeePassXC) and the location of the home directory for the `.json` file. Both of these are explained, however, and can be modified to suit one's needs. [Click here](#tldr) to skip the explanation.


## The Problem {#the-problem}

KeePassXC uses a socket to communicate with browser extensions which is located at one of two locations. This is a common method for two different pieces of software to communicate and for our purposes we view it a simple file that we need to read and write to.

-   Older versions of KeePassXC use `$XDG_RUNTIME_DIR/kpxc_server`
-   Newer versions use `$XDG_RUNTIME_DIR/app/org.keepassxc.KeePassXC/org.keepassxc.KeePassXC.BrowserServer`

Assuming you have an up-to-date flatpak, the socket should be located at the newer path.

The browser extension then uses [Native Messaging](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Native_messaging) to run a binary that communicates with the socket. Due to sandboxing, the flatpak version of Firefox has two problems to solve before it can communicate with KeePassXC: socket access and socket communication.


### Socket Access {#socket-access}

It needs access to the KeePassXC socket. Without the flatpak sandbox, Firefox has access to everything in `$XDG_RUNTIME_DIR` as everything in the runtime directory is owned by the same user running Firefox. This can be verified using ls.

```nil
ls -l $XDG_RUNTIME_DIR
```

```text
total 4
drwx------. 9 example_user example_user 180 Oct  1 11:45 app/
...
srwxr-xr-x. 1 example_user example_user   0 Oct  1 10:57 wayland-0=
-rw-r-----. 1 example_user example_usr   0 Oct  1 10:57 wayland-0.lock
```

This output shows that `example_user` owns the files in `$XDG_RUNTIME_DIR`, so as long as `example_user` runs Firefox, that Firefox process will be able to access the contents of the `$XDG_RUNTIME_DIR`. Specifically, the the permissions of the `app/` directory show that `example_user` and only `example_user` will be able to read from and write into the directory.

Flatpak Firefox does run with the same user, so this isn't an issue. The problem is that flatpak does sandboxes applications which prevents them from accessing things they shouldn't. This is good from a security perspective; it becomes more difficult to exploit vulnerabilities in an application. If a program doesn't _need_ something to function properly, it shouldn't be given access to it. This includes sockets provided by other applications. Flatpak has no way of knowing what software should be allowed to communicate with eachother unless it is directly told. Normally this is done by through [flatpak manifests](https://docs.flatpak.org/en/latest/manifests.html) by whoever packages the flatpak application. This way the application is able to function as it normally would with just enough access and permissions needed to run.

In our case we are using an extension that is not installed by default which uses a socket that Firefox does not normally need access to. This means we have to tell flatpak that Firefox is allowed to access the KeePassXC socket.


### Socket Communication {#socket-communication}

Native Messaging involves running an external program designed to communicate with Firefox and whatever software that wants to communicate with Firefox. For the KeePassXC browser plugin, this is normally provided by KeePassXC itself. It comes with the `keepassxc-proxy` binary which facilitates the communication between KeePassXC and browser extensions. Our Firefox flatpak can't access this binary, so we need to find another way.

There are methods to run host commands from within a flatpak sandbox, but these methods essentially make the sandbox useless. It is an option, but a web browser is one application that should be sandboxed so this method will not be used.


## The Solution {#the-solution}


### Accessing the Socket {#accessing-the-socket}

Fixing this is as simple as adding a flatpak override to Firefox. First run KeePassXC, enable Browser Integration if you haven't already, and verify the location of the socket.

```sh
ls $XDG_RUNTIME_DIR/app/org.keepassxc.KeePassXC
```

```text
org.keepassxc.KeePassXC.BrowserServer=
```

If the file or directory does not exist, verify that Browser Integration is enabled in KeePassXC. If it is, then look around `$XDG_RUNTIME_DIR` for the socket. It may be located at the old location.

Now we need to add the override:

```sh
sudo flatpak override \
    --filesystem=xdg-run/app/org.keepassxc.KeePassXC/org.keepassxc.KeePassXC.BrowserServer \
     org.mozilla.firefox
```

There are a few things to note with this command:

1.  We use `xdg-run` as part of the path. This is flatpak's equivalent of the `$XDG_RUNTIME_DIR` environment variable.
2.  If your socket is at a different location, use that location. For the old socket location it would be `xdg-run/keepassxc_server`.
    1.  The filesystem path is **not** marked readonly by appending `:ro`. If you don't know what this means, you can ignore it, but some other guides add `:ro` to their override commands. This doesn't make sense, as the browser needs to write to the socket to send information to KeePassXC. How else will KeePassXC know what username/password is requested? In any case, if you append `:ro` it won't work.


### Verifying Socket Access {#verifying-socket-access}

We can verify that the override is working by entering the Firefox flatpak sandbox and looking for the socket.

```sh
flatpak run --command=sh org.mozilla.firefox
```

```sh
[📦 org.mozilla.firefox ~]$
```

This gives you a shell inside of the sandbox. Every file that the flatpak has access will show up in the sandbox. To verify the override is working, simply check the contents of `$XDG_RUNTIME_DIR` while using the flatpak sandox shell

```sh
ls $XDG_RUNTIME_DIR/app
```

```text
org.keepassxc.KeePassXC  org.mozilla.firefox
```

or for the older socket location

```sh
ls $XDG_RUNTIME_DIR/kpxc_server
```

```text
/run/user/1000/kpxc_server
```

If the KeePassXC folder or socket doesn't show up, exit the sandbox and verify the override:

```sh
exit
flatpak override --show org.mozilla.firefox
```

```text
[Context]
filesystems=xdg-run/app/org.keepassxc.KeePassXC/org.keepassxc.KeePassXC.BrowserServer;
```

If the override is missing or points to the wrong location, you can try again after resetting:

```sh
sudo flatpak override --reset org.mozilla.firefox
```


### Setting Up Native Messaging {#setting-up-native-messaging}

Native Messaging allows a Firefox extension to run a command on the host. For the KeePassXC extension, this is normally handled by KeePassXC itself. However, Firefox is unable to access the KeePassXC binary due to the sandbox. Instead, we use a statically compiled binary for communicating with KeePassXC through the socket.

The Firefox configuration folder is normally located at `~/.mozilla`, but each flatpak app has its own home folder located at `~/.var/app`, so the folder we are looking for is `~/.var/app/org.mozilla.firefox/.mozilla`. We need to create a new directory for Native Messaging and setup Native Messaging for KeePassXC.

```sh
mkdir -p ~/.var/app/org.mozilla.firefox/.mozilla/native-messaging-hosts
```

In that directory, create a file named `org.keepassxc.keepassxc_browser.json` with the following content:

```js
{
    "allowed_extensions": [
	"keepassxc-browser@keepassxc.org"
    ],
    "description": "KeePassXC workaround for flatpak Firefox",
    "name": "org.keepassxc.keepassxc_browser",
    "path": "/var/home/REPLACE_WITH_USERNAME/.mozilla/native-messaging-hosts/keepassxc-proxy",
    "type": "stdio"
}
```

Be sure to replace your username with the correct value. Note that the path specified is the path relative to the flatpak sandbox, so we use `.mozilla` instead of `.var/app/org.mozilla.firefox/.mozilla`. Note that the path starts with `/var/home` instead of `/home`. This is because on Fedora Silverblue `/home` is a symbolic link to `/var/home`. The symbolic link doesn't exist in the sandbox, so there is no `/home` folder in the sandbox and we have to use the actual path. If you aren't using Silverblue, remove the `/var` part.

You can also use the following command to create the file from the command line:

```sh
cat <<EOF > $HOME/.var/app/org.mozilla.firefox/.mozilla/native-messaging-hosts/org.keepassxc.keepassxc_browser.json
{
    "allowed_extensions": [
	"keepassxc-browser@keepassxc.org"
    ],
    "description": "KeePassXC workaround for flatpak Firefox",
    "name": "org.keepassxc.keepassxc_browser",
    "path": "$HOME/.mozilla/native-messaging-hosts/keepassxc-proxy",
    "type": "stdio"
}
EOF
```


### Getting the Native Messaging Executable {#getting-the-native-messaging-executable}

The `.json` file we created earlier specifies the path to an executable named `keepassxc-proxy` which doesn't currently exist. Although I could provide you with the binary I previously compiled, you shouldn't trust a random binary from the internet, especially one that is supposed to interact with your password manager. So the only choice is to compile it yourself.

We are going to build the rust implementation of `keepassxc-proxy` located [here](https://github.com/varjolintu/keepassxc-proxy-rust). It is a small program, and it shouldn't be too hard to verify that it isn't doing anything nefarious even if you don't know rust. If you don't have `rustup` installed, follow the instructions [here](https://rustup.rs/) to do so.

To avoid incompatibilities, we want to compile a completely static version of `keepassxc-proxy` using [musl](https://en.wikipedia.org/wiki/Musl). Hence, we add musl as a target for rust.

```sh
rustup target add x86_64-unknown-linux-musl
```

After that completes, clone the repository and build the executable.

```sh
git clone https://github.com/varjolintu/keepassxc-proxy-rust
cd keepassxc-proxy-rust
RUSTFLAGS='-C link-arg=-s' cargo build --release \
    --target x86_64-unknown-linux-musl
```

If this succeeds, we can then copy it to the Native Messaging directory

```sh
cp target/x86_64-unknown-linux-musl/release/keepassxc-proxy \
    ~/.var/app/org.mozilla.firefox/.mozilla/native-messaging-hosts
```

Assuming everything has worked out, the KeePassXC Add-On should be able to communicate with KeePassXC. You whave to run KeePassXC before starting Firefox, or else Firefox will throw an error in the [Browser Console](https://firefox-source-docs.mozilla.org/devtools-user/browser_console/index.html). If things still aren't working, look in the Browser Console for any errors relating to Native Messaging.


## TLDR {#tldr}


### Give Firefox Flatpak Socket Access {#give-firefox-flatpak-socket-access}

```sh
sudo flatpak override \
    --filesystem=xdg-run/app/org.keepassxc.KeePassXC/org.keepassxc.KeePassXC.BrowserServer \
    org.mozilla.firefox
```


### Enable Native Messaging in Firefox {#enable-native-messaging-in-firefox}

```sh
mkdir -p $HOME/.var/app/org.mozilla.firefox/native-messaging-hosts
cat <<EOF > $HOME/.var/app/org.mozilla.firefox/.mozilla/native-messaging-hosts/org.keepassxc.keepassxc_browser.json
  {
      "allowed_extensions": [
	  "keepassxc-browser@keepassxc.org"
      ],
      "description": "KeePassXC workaround for flatpak Firefox",
      "name": "org.keepassxc.keepassxc_browser",
      "path": "$HOME/.mozilla/native-messaging-hosts/keepassxc-proxy",
      "type": "stdio"
  }
  EOF
```


### Build Proxy Binary {#build-proxy-binary}

```sh
rustup target add x86_64-unknown-linux-musl
git clone https://github.com/varjolintu/keepassxc-proxy-rust
cd keepassxc-proxy-rust
RUSTFLAGS='-C link-arg=-s' cargo build --release \
    --target x86_64-unknown-linux-musl
cp target/x86_64-unknown-linux-musl/release/keepassxc-proxy \
    ~/.var/app/org.mozilla.firefox/.mozilla/native-messaging-hosts
```


## References {#references}

There are a few guides floating around that show how to fix these issues. Two of them located [here](https://discourse.flathub.org/t/how-to-run-firefox-and-keepassxc-in-a-flatpak-and-get-the-keepassxc-browser-add-on-to-work/437) and [here](https://ebsteblog.wordpress.com/2022/06/28/connecting-flatpak-firefox-and-keepassxc/) are good references, however neither of them worked for me without modification. There is also the KeePassXC Github [issue](https://github.com/flathub/org.keepassxc.KeePassXC/issues/29) about it here.
