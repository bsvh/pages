---
title: "OpenPGP Keys"
toc: False
---


## Current
[pubkey.asc](https://www.vastactive.com/pubkey.asc)

```
pub   ed25519 2022-05-27 [C]
      6CD2 A117 F3CF 681C E3A8  9A47 462B 2011 B5D4 A592
uid           [ultimate] Brendan Van Hook <brendan@vastactive.com>
sub   ed25519 2022-05-27 [A] [expires: 2023-05-27]
sub   cv25519 2022-05-27 [E] [expires: 2023-05-27]
```

## Archived
```
pub   rsa4096 2019-03-26 [C]
      6D4E 2A4A 235C ADA9 7206  9EAB 37BE 6B81 8340 5D6D
uid           [ultimate] Brendan Van Hook <brendan@vastactive.com>
uid           [ultimate] Brendan Van Hook <bvanhook@terpmail.umd.edu>
uid           [ultimate] Brendan Van Hook <me@bsvh.me>
sub   rsa4096 2019-03-26 [S] [expires: 2024-04-30]
sub   rsa4096 2019-03-26 [E] [expires: 2024-04-30]
```
